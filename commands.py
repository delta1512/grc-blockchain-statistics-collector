import walletops as w
import maths as m


DEFAULT_BLOCKS = 40

async def process_args(args):
    if len(args) == 0:
        current_block = await w.get_current_block()
        return [current_block - DEFAULT_BLOCKS, current_block, False]
    elif len(args) == 1:
        current_block = await w.get_current_block()
        return [int(args[0]), current_block, False]
    elif len(args) == 2:
        return [int(args[0]), int(args[1]), False]
    else:
        return [int(args[0]), int(args[1]), bool(args[2])]

async def getdiff(args):
    results = []
    for blk_height in range(args[0], args[1]+1):
        data = await w.get_block_data(blk_height)
        results.append([blk_height, data['difficulty']])
    return args, results

async def getdiffma(args):
    null, diffs = await getdiff(args)
    diffs = [val[1] for val in diffs]
    results = [[args[0]+i, ma] for i, ma in enumerate(m.get_moving_average(diffs))]
    return args, results

async def getcpids(args):
    cpids = set()
    for blk_height in range(args[0], args[1]+1):
        data = await w.get_block_data(blk_height)
        cpids.add(data['staker'])
    results = [list(cpids.difference({None}))]
    return args, results

async def getaddresses(args):
    addrs = set()
    for blk_height in range(args[0], args[1]+1):
        tx_data = await w.get_txs_from_block(blk_height)
        for tx in tx_data:
            [addrs.add(addr) for addr in tx['addresses']]
    results = [list(addrs)]
    return args, results

async def gettxvolume(args):
    results = []
    for blk_height in range(args[0], args[1]+1):
        results.append([blk_height, sum([tx_data['volume'] for tx_data in await w.get_txs_from_block(blk_height) if (not tx_data['isstake'] or args[2])])])
    return args, results

async def getminerinfo(args):
    results = []
    for blk_height in range(args[0], args[1]+1):
        data = await w.get_block_data(blk_height)
        # For POS only flag
        if args[2] and data['staker'] != 'INVESTOR':
            data['mint'] = 0
        results.append([blk_height, data['staker'], data['staker_address'], data['mint']])
    return args, results

async def getcombinedstats(args):
    results = []
    for blk_height in range(args[0], args[1]+1):
        row = []
        blk_data = await w.get_block_data(blk_height)
        tx_data = await w.get_txs_from_block(blk_height)
        # Height
        row.append(blk_height)
        # Block time
        row.append(blk_data['time'])
        # Difficulty
        row.append(blk_data['difficulty'])
        # Miner CPID
        row.append(blk_data['staker'])
        # Miner address
        row.append(blk_data['staker_address'])
        # Mint
        row.append(blk_data['mint'])
        # Client version
        row.append(blk_data['staker_version'])
        # TX volume
        row.append(sum([tx_data['volume'] for tx_data in await w.get_txs_from_block(blk_height)]))
        results.append(row)
    return args, results
