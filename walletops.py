import aiohttp
import json

import config as c


NET_W_MULT = 9544517.41


# Code snippet from https://gitlab.com/delta1512/grc-wallet-bot
async def query(cmd, params):
    if not all([isinstance(cmd, str), isinstance(params, list)]):
        raise Exception('Invalid arguments provided')
    command = json.dumps({'method' : cmd, 'params' : params})
    async with aiohttp.ClientSession() as session:
        async with session.post(c.HOST, data=command, headers={'content-type': "application/json", 'cache-control': "no-cache"}, auth=aiohttp.BasicAuth(c.USR, password=c.PASS)) as resp:
            # split up steps as a workaround for resp.json() not working
            # with output from `gettransaction` for superblock transactions
            # (aiohttp trying to decode <BINARY> part in hashboinc string)
            raw = await resp.read()
            text = raw.decode('utf-8', 'ignore')
            response = json.loads(text)
    if response['error'] != None:
        raise Exception('Error field was not null when communicating with GRC client. Error: {}'.format(response['error']))
    else:
        return response['result']


async def get_current_block():
    return await query('getblockcount', [])


async def get_block_data(height):
    results = {}
    block_data = await query('getblockbynumber', [height])
    results = {
        'height'            :       height,
        'hash'              :       block_data['hash'],
        'time'              :       block_data['time'],
        'difficulty'        :       block_data['difficulty'],
        'nWeight'           :       NET_W_MULT * block_data['difficulty'],
        'nTXs'              :       len(block_data['tx']),
        'mint'              :       block_data['mint'],
        'superblock'        :       block_data['IsSuperBlock'] == 0,
        'staker'            :       block_data['CPID'],
        'staker_address'    :       block_data['GRCAddress'],
        'staker_version'    :       block_data['ClientVersion']
    }
    return results


async def get_tx_data(txid):
    tx_data = await query('gettransaction', [txid])
    vouts = tx_data['vout']
    #get tx volume
    volume = sum([output['value'] for output in vouts])
    #get unique addresses
    addresses = set()
    for output in vouts:
        try:
            for addr in output['scriptPubKey']['addresses']:
                addresses.add(addr)
        except:
            continue
    #check for stake
    stake = False
    for output in vouts:
        stake = stake or (output['scriptPubKey']['type'] == 'nonstandard')

    return {'time'      :       tx_data['time'],
            'volume'    :       volume,
            'addresses' :       addresses,
            'isstake'   :       stake}


async def get_txs_from_block(height):
    block_data = await query('getblockbynumber', [height])
    return [await get_tx_data(txid) for txid in block_data['tx']]
