import asyncio
import csv

import commands as cmds


LOOP = asyncio.get_event_loop()

command = ''
while command != 'exit':
    command = input('GRCstats.py$ ')
    args = command.lower().split()[1:]
    command = command.lower().split()[0]
    if command in dir(cmds):
        func = eval('cmds.' + command)
        args, results = LOOP.run_until_complete(func(LOOP.run_until_complete(cmds.process_args(args))))
        with open('{}-{} to {}.csv'.format(command, args[0], args[1]), 'w') as csvfile:
            w = csv.writer(csvfile, delimiter=',')
            for blk in results:
                w.writerow(blk)
    else:
        print('Invalid command. See README.md for help.')
