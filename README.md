# GRC Statistic Scraper

#### Made by Delta for use in the Gridcoin Bluepaper developments

A set of tools that can be used to scrape basic statistics from the Gridcoin blockchain. The script will run and output a csv file to the local directory based on the command run.

## Basic Manual:

```
All arguments are optional.
Block value arguments are inclusive.
If all block values are omittied, gets stats for the last 40 blocks.
If final block value is ommitted, uses the client latest block instead.

Commands:
    getdiff [initial] [final]
        Gets the difficulty for each block from [initial] block to [final] block.

    getdiffma [initial] [final]
        Gets the moving average from [initial] block to [final] block. If arguments
        ommitted, gets the 40-block MA for difficulty.

    getcpids [initial] [final]
        Gets the unique stakers and percent recearch rewards from [initial] block
        to [final] block.

    getaddresses [initial] [final]
        Gets the unique addresses that appeared on the chain from [initial] block
        to [final] block.

    gettxvolume [initial] [final] [staking]
        Gets the total volume of transacted coins within each block from
        [initial] block to [final] block. If [staking] is set to true, staking
        volume will also be considered in the data.

    getminerinfo [initial] [final] [POS only]
        From [initial] block to [final] block, this will collect: research/stake
        reward per block, miner address, miner cpid, collected fees.

    getcombinedstats [initial] [final]
        Gets a large set of generalised statistics similar to a contemporary
        block explorer. Stats include:
        - miner CPID
        - miner address
        - minted research rewards
        - transaction values
        - volume of transactions
        - all unique addresses
        - block difficulty
        - block time
        - client version of miner/staker
```
