def get_moving_sum(terms):
    final = []
    s = 0
    for t in terms:
        s += t
        final.append(s)
    return final

def get_moving_average(terms):
    final = []
    sums = get_moving_sum(terms)
    for i, t in enumerate(sums):
        final.append(sums[i]/(i+1))
    return final
